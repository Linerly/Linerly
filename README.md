# 👋 Hello there!

Welcome to my GitLab profile! 🦊

<details>
<summary>What I do</summary>
  <p>I mostly translate things that I use.</p>
  <p>...and sometimes fixing things in pages and documentations as well.</p>
</details>

I still don't really have anything on GitLab, other than my contributions to various projects on GitLab.
[Check out some of my repositories on Codeberg!](https://codeberg.org/Linerly)

### [<img src="https://linerly.xyz/media/core/icon-optimized.svg" width="16" height="16"> My Website](https://linerly.xyz/)